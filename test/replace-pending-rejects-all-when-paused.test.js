import { ActionQueue } from '../src/index.js';
import { TimedPromise } from './utils/TimedPromise.js';
import _ from 'underscore';

import { test, expect } from 'vitest';

test("Cancelling with replace_pending, rejects only pending actions", async () => {
  expect.assertions(2);

  const ACTION_TIME = 50;  // ms

  let queue = new ActionQueue();
  queue.pause();

  let promise1 = expect(queue.append(() => TimedPromise(ACTION_TIME))).rejects.toThrow("Action was cancelled");
  let promise2 = expect(queue.append(() => TimedPromise(ACTION_TIME))).rejects.toThrow("Action was cancelled");

  // replace_pending rejects all because we're paused.
  queue.replace_pending(() => TimedPromise(ACTION_TIME));

  await promise1;
  await promise2;

});
